<?php

namespace Majframe\Composer;

use Composer\Package\PackageInterface;
use Composer\Installer\LibraryInstaller;

class ModuleInstaller extends LibraryInstaller
{
    /**
     * {@inheritDoc}
     */
    public function getInstallPath(PackageInterface $package)
    {
        $name = explode('/', $package->getPrettyName())[1];
        if ('module-' !== substr($name, 0, 7)) {
            throw new \InvalidArgumentException(
                'Unable to install module, majframe modules '
                .'should always start their package name with '
                .'"module-"'
            );
        }

        return 'app/Modules/'.substr($name, 7);
    }

    /**
     * {@inheritDoc}
     */
    public function supports($packageType)
    {
        return 'majframe-module' === $packageType;
    }
}